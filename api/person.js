const router = require('express').Router();

router.get('/', async (req,res)=>{
  res.send([
    {
      name:"Foo"
    },
    {
      name:"Bar"
    }
  ])
});

router.post('/', async (req,res) => {
  res.send(req.body);
})

module.exports = router;