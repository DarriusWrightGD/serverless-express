# Serverless Express

This project uses [aws-serverless-express](https://github.com/awslabs/aws-serverless-express) in combination with [sam local](https://github.com/awslabs/aws-sam-cli) to create
an express api.

To test run `npm start`

This will start on `http://localhost:3000` and there are two endpoints that you can call.

- `/person`: [GET, POST]
- `/healthcheck`: [GET]
