const app = require('express')();
const bodyParser = require('body-parser');

const healthcheck = require('./api/healthcheck');
const person = require('./api/person');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/healthcheck', healthcheck);
app.use('/person', person);

module.exports = app;